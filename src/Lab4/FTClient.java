package Lab4;

import java.io.*;


public class FTClient {
	private int serverPort=6050;
	private BufferedReader userInput;
	private CommunicationInterface ftp = new UDPFileTransfer();
	
	public void run() {
		String command = "";
		ftp.initializeClient("localhost",serverPort);
		try {
			userInput = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("COMMANDS: get,filename");
			System.out.println("\t  put,filename");
			System.out.println("\t  quit");
			System.out.println("\t  example: put,data.txt");
		} catch (Exception e) {

		}
		while (true) {

			System.out.print("ftp>");
			command = getCommand();
			try {
				ftp.sendCommand(command);
			} catch (Exception e) {
				ftp.error("Could not write out to server: " + e);
			}
			if (command.toLowerCase().equals("quit"))
				break;
			readCommand(command);
		}
	}

	public String getCommand() {
		String command = "";
		try {
			command = userInput.readLine();
			System.out.println("got Command: " + command);
		} catch (Exception e) {
			ftp.error("Could not read input: " + e);
		}
		return command;

	}

	public void sendFile(String filePath) {
		filePath = new String("./Client/Send/" + filePath);
		File theFile = new File(filePath);
		if (theFile.exists())
			ftp.sendFile(filePath);
	}

	public void receiveFile(String filePath) {
		filePath = new String("./Client/Receive/" + filePath);
		ftp.receiveFile(filePath);
	}

	public void readCommand(String s) {
		String selectedAction[];
		try {
			selectedAction = s.split(",");
			if (selectedAction.length != 2) {
				ftp.error("Invalid number of arguments");
			}
			switch (selectedAction[0].toLowerCase()) {
			case "put":
				System.out.println("Client: is sending");
				sendFile(selectedAction[1]);
				break;
			case "get":
				System.out.println("Client: is receiving");
				receiveFile(selectedAction[1]);
				break;
			default:
				ftp.error("Invalid Input");
			}
		} catch (Exception e) {
			ftp.error("Invalid input: " + e);
		}
	}
}
