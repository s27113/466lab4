package Lab4;

public class FTServer {

	private int serverPort = 6070;
	private CommunicationInterface ftp = new UDPFileTransfer();
	private boolean txComplete = false;

	public void run() {
		System.out.println("FT Server Started");

		ftp.initializeServer(serverPort);
		System.out.println("Server is waiting for connection on Port: "
				+ serverPort);
		while (!txComplete) {
			String command = "";
			try {
				command = ftp.getCommand();
				System.out.println("Server recieved command: " + command);
			} catch (Exception e) {
				ftp.error("Could not read input: " + e);
			}
			readCommand(command);
		}

	}

	public void sendFile(String filePath) {
		filePath = new String("./Server/Send/" + filePath);
		ftp.sendFile(filePath);
	}

	public void receiveFile(String filePath) {
		filePath = new String("./Server/Receive/" + filePath);
		ftp.receiveFile(filePath);
	}

	public void readCommand(String s) {
		String selectedAction[];
		try {
			selectedAction = s.split(",");
			if (selectedAction.length != 2) {
				if (selectedAction[0].toLowerCase().equals("quit")) {
					ftp.closeConnection();
					txComplete = true;

				} else {
					ftp.error("Invalid number of arguments");
				}
			} else {
				switch (selectedAction[0].toLowerCase()) {
				case "put":
					System.out.println("Server: is receiving");
					receiveFile(selectedAction[1]);
					break;
				case "get":
					System.out.println("Server: is sending");
					sendFile(selectedAction[1]);
					break;
				default:
					ftp.error("Invalid Input");
				}
			}
		} catch (Exception e) {
			ftp.error("Invalid input: " + e);
		}
	}

}