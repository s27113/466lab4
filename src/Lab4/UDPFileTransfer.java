package Lab4;

import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class UDPFileTransfer implements CommunicationInterface {
	private ByteBuffer buffer, byteMagic;
	private DatagramSocket socket;
	private InetAddress destAddress;
	private int destPort, srcPort;
	private boolean isServer = false;
	private DatagramPacket history, ackPacket, packet;

	/**
	 * The short timeout
	 */
	public static final int CLIENT_SOCKET_TIMEOUT_TIME = 500;
	
	/**
	 * The long timeout
	 */
	public static final int SERVER_SOCKET_TIMEOUT_TIME = 5000;
	
	/**
	 * The size of the buffer
	 */
	public static final int BUFFER_SIZE = 1200;
	
	/**
	 * The size of the ack packet
	 */
	public static final int ACK_PACKET_SIZE = Integer.BYTES;
	
	/**
	 * The total number of timeout before giving up
	 */
	public static final int TOTAL_CLIENT_TIMEOUTS_BEFORE_QUIT = 10;
	
	/**
	 * The size of the packet header
	 */
	public static final int DATA_PACKET_HEADER_SIZE = Integer.BYTES + 1;

	@Override
	public void sendFile(String filePath) {
		if (isServer) {
			serverSendFile(filePath);
		} else {
			try {
				clientSendFile(filePath);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void receiveFile(String filePath) {
		if (isServer) {
			try {
				serverReceiveFile(filePath);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			try {
				clientReceiveFile(filePath);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}// receiveFile()

	@Override
	public void sendCommand(String command) {
		buffer = ByteBuffer.allocate(command.length());
		buffer.put(command.getBytes());
		history = new DatagramPacket(buffer.array(), buffer.array().length, destAddress, destPort);
		try {
			socket.send(history);
		} catch (Exception e) {
			error("Unable to send command " + e);
		}
	}

	@Override
	public String getCommand() {
		String command = "";
		byte[] temp = new byte[512];
		DatagramPacket packet = new DatagramPacket(temp, temp.length);
		try {
			socket.receive(packet);
			System.out.println("Got a packet");
		} catch (Exception e) {
			error("Error recieving packet: " + e);
		}
		buffer = ByteBuffer.allocate(packet.getLength());
		buffer.put(Arrays.copyOf(packet.getData(), packet.getLength()));
		buffer.flip();
		command = new String(buffer.array());
		destAddress = packet.getAddress();
		destPort = packet.getPort();
		return command;
	}

	public void error(String e) {
		System.err.println("Error: " + e);
	}

	@Override
	public void initializeServer(int sourcePort) {
		isServer = true;
		srcPort = sourcePort;
		try {
			socket = new DatagramSocket(srcPort);
		} catch (SocketException e) {
			error("error initializing the server: " + e);
		}
	}

	@Override
	public void initializeClient(String address, int destinationPort) {
		destPort = destinationPort;
		try {
			socket = new DatagramSocket();
			destAddress = InetAddress.getByName(address);
		} catch (Exception e) {
			error("error initializing the server: " + e);
		}
	}

	@Override
	public void closeConnection() {
	}

	private void removeOldFiles(String filePath) {
		File thisFile = new File(filePath);
		if (thisFile.exists()) {
			thisFile.delete();
		}
	}

	/**
	 * This method receives a file on the client side
	 * 
	 * @param filePath
	 *            Where we are going to write the file
	 * @throws IOException
	 */
	@SuppressWarnings("resource")
	private void clientReceiveFile(String filePath) throws IOException {
		// File writer to build the file
		FileOutputStream fileWriter;
		// Some buffer for us to use
		byte[] buffer = new byte[BUFFER_SIZE], fileBuffer;
		// Some packets for us to use
		DatagramPacket replyPacket, receivedPacket = new DatagramPacket(buffer, buffer.length);
		// Keep track of packet number and total timeouts
		int packetNumber = 0, totalTimeouts = 0;
		// Check if it's the last packet
		byte isLastPacket;
		// Clean the filepath
		filePath = filePath.replaceAll("\u0000", "");
		// Get the path of the file we are saving
		Path savePath = Paths.get(System.getProperty("user.dir"), filePath);
		// Clear old files
		removeOldFiles(filePath);
		try {
			// Create a file to write the data into
			System.out.println("Creating file: " + filePath);
			Files.createFile(savePath);
		} catch (IOException e) {
			error("Unable to save generated file at " + savePath + " " + e);
			return;
		}
		try {
			// Open the file we just created with the filewriter
			System.out.println("Writing to file");
			fileWriter = new FileOutputStream(filePath);
		} catch (Exception e) {
			error("Error writing file: " + e);
			return;
		}
		// Set timeout
		setTimeout(CLIENT_SOCKET_TIMEOUT_TIME);
		while (true) {
			try {
				System.out.println("Attempting to receive packet");
				// Allocate buffer
				byteMagic = ByteBuffer.allocate(BUFFER_SIZE);
				// Receive packet
				socket.receive(receivedPacket);
				// Restore timeout number
				totalTimeouts = 0;
				// Copy over data and flip to restore correct order
				byteMagic.put(Arrays.copyOf(receivedPacket.getData(), receivedPacket.getLength()));
				byteMagic.flip();
				// Extract header
				packetNumber = byteMagic.getInt();
				isLastPacket = byteMagic.get();
				System.out.println("Recieved packet #" + packetNumber + ", the isLastPacket value is: " + isLastPacket);
				// Filebuffer to write to file
				fileBuffer = new byte[byteMagic.remaining()];
				// copy bytes into filebuffer
				byteMagic.get(fileBuffer);
				System.out.println("Writing packet #" + packetNumber + " to file");
				// Write bytes to file
				fileWriter.write(fileBuffer, 0, fileBuffer.length);
				// Reallocate buffer
				byteMagic = ByteBuffer.allocate(ACK_PACKET_SIZE);
				// Create and send acknowledgement packet, saving it to history
				byteMagic.putInt(packetNumber);
				history = new DatagramPacket(byteMagic.array(), byteMagic.array().length, receivedPacket.getAddress(),
						receivedPacket.getPort());
				socket.send(history);
				// If it's the last packet
				if (isLastPacket != 0) {
					System.out.println("Transfer Complete!");
					// Close the filewriter and disable timeouts
					fileWriter.close();
					disableTimeout();
					break;
				}
			} catch (SocketTimeoutException e) { // Timeout occurred
				// Increment timeout
				totalTimeouts++;
				// Initial data not received
				if (packetNumber == 0)
					System.out.println("No data received, resending request");
				// Packet along the way not received
				else
					System.out.println("Packet #" + Integer.toString(packetNumber + 1)
							+ " not received, resending Ack #" + packetNumber);
				// Max number of timeouts reached
				if (totalTimeouts == TOTAL_CLIENT_TIMEOUTS_BEFORE_QUIT) {
					System.out.println("Maximum number of timeouts exceeded, transfer failed!");
					// Close the filewriter, delete partially downloaded file,
					// and disable timeouts
					fileWriter.close();
					removeOldFiles(filePath);
					disableTimeout();
					break;
				}
				// Resend history
				socket.send(history);
			}
		}
	}

	/**
	 * This method sends a file from the server to the client
	 * 
	 * @param filePath
	 *            The path of the file to send
	 */
	private void serverSendFile(String filePath) {
		int packetNumber = 1; // packet number starts at 1
		FileInputStream fileReader = null;
		boolean lastSegment = false, isLastPacketThrough = false;

		try {
			filePath = filePath.replace("\u0000", "");
			File theFile = new File(filePath);
			try {
				fileReader = new FileInputStream(theFile);
			} catch (IOException e) {
				error("Failed to load file");
				return;
			}
		} catch (Exception e) {
			error("File reading error sending a chunk");
			return;
		}
		setTimeout(SERVER_SOCKET_TIMEOUT_TIME);
		while (true) {
			isLastPacketThrough = false;
			try {
				lastSegment = prepareBuffer(packetNumber, fileReader.available());
				packet = bufferToPacket(fileReader);
				do {
					socket.send(packet);
					ackPacket = new DatagramPacket(new byte[ACK_PACKET_SIZE], ACK_PACKET_SIZE);
					socket.receive(ackPacket);
					if (getAckNumber(ackPacket) == packetNumber) {
						isLastPacketThrough = true;
						packetNumber++;
					}
				} while (!isLastPacketThrough);
				if (lastSegment) {
					fileReader.close();
					disableTimeout();
					return;
				}
			} catch (SocketTimeoutException se) {
				System.out.println("Client disapeared or network is down");
				disableTimeout();
				return;
			} catch (IOException e) {
				disableTimeout();
				error("Error transmitting packet: " + e);
				return;
			}
		} // while
	}

	/**
	 * This method receives a file on the server's behalf
	 * 
	 * @param filePath
	 *            Where we are going to write the file
	 * @throws IOException
	 */
	private void serverReceiveFile(String filePath) throws IOException {
		// Filewriter to write our file
		FileOutputStream fileWriter;
		// Some buffers for us to use
		byte[] buffer = new byte[BUFFER_SIZE], fileBuffer, cmdAck = { 0, 0, 0, 0 };
		// Some packets for us to use
		DatagramPacket receivedPacket = new DatagramPacket(buffer, buffer.length);
		// Integers to keep track of the current packet number and timeout
		int packetNumber = 0, totalTimeouts = 0;
		// Byte to check if its the last packet
		byte isLastPacket;
		// Boolean to check if the ack has been received on client side
		boolean ackReceived = false;
		// Clean the file path
		filePath = filePath.replaceAll("\u0000", "");
		// Get the save path
		Path savePath = Paths.get(System.getProperty("user.dir"), filePath);
		// Remove old files
		removeOldFiles(filePath);
		// Send the ack
		history = new DatagramPacket(cmdAck, ACK_PACKET_SIZE, destAddress, destPort);
		System.out.println("Sending Ack");
		socket.send(history);
		try {
			// Create a file to write the data into
			System.out.println("Creating file: " + filePath);
			Files.createFile(savePath);
		} catch (IOException e) {
			error("Unable to save generated file at " + savePath + " " + e);
			return;
		}
		try {
			// Open the file with the filewriter
			System.out.println("Writing to file");
			fileWriter = new FileOutputStream(filePath);
		} catch (Exception e) {
			error("Error writing file: " + e);
			return;
		}
		// Set the timeout
		setTimeout(CLIENT_SOCKET_TIMEOUT_TIME);
		while (true) {
			try {
				System.out.println("Attempting to receive packet");
				// Allocate the buffer
				byteMagic = ByteBuffer.allocate(BUFFER_SIZE);
				// Receive the packet
				socket.receive(receivedPacket);
				// Reset the timeouts
				totalTimeouts = 0;
				// Copy over data and flip it to restore order
				byteMagic.put(Arrays.copyOf(receivedPacket.getData(), receivedPacket.getLength()));
				byteMagic.flip();
				// Extract the first part of the packet header
				packetNumber = byteMagic.getInt();
				// If the packet is a command packet, resend the ack
				if (ackReceived == false && packetNumber != 1) {
					System.out.println("Recieved command again, resending ack");
					socket.send(history);
					// Check if ack went through
					continue;
				} else if (ackReceived == false && packetNumber == 1) {
					// Otherwise ack went through
					ackReceived = true;
				}
				// Extract second part of header
				isLastPacket = byteMagic.get();
				System.out.println("Recieved packet #" + packetNumber + ", the isLastPacket value is: " + isLastPacket);
				// Allocate the file buffer
				fileBuffer = new byte[byteMagic.remaining()];
				// Copy over data into buffer
				byteMagic.get(fileBuffer);
				System.out.println("Writing packet #" + packetNumber + " to file");
				// Write data to file
				fileWriter.write(fileBuffer, 0, fileBuffer.length);
				// Reallocate buffer
				byteMagic = ByteBuffer.allocate(ACK_PACKET_SIZE);
				// Create and send reply packet, saving it to history
				byteMagic.putInt(packetNumber);
				history = new DatagramPacket(byteMagic.array(), byteMagic.array().length, receivedPacket.getAddress(),
						receivedPacket.getPort());
				socket.send(history);
				// Last packet
				if (isLastPacket != 0) {
					System.out.println("Transfer Complete!");
					// Close file writer and disable timeouts
					fileWriter.close();
					disableTimeout();
					break;
				}
			} catch (SocketTimeoutException e) { // Timeout occurred
				// Increment timeouts
				totalTimeouts++;
				// Initial data not received
				if (packetNumber == 0)
					System.out.println("No data received, resending request");
				// Data along the way lost
				else
					System.out.println("Packet #" + Integer.toString(packetNumber + 1)
							+ " not received, resending Ack #" + packetNumber);
				// Max timeouts exceeded
				if (totalTimeouts == TOTAL_CLIENT_TIMEOUTS_BEFORE_QUIT) {
					System.out.println("Maximum number of timeouts exceeded, transfer failed!");
					// Close file writer, remove failed file, and disable
					// timeouts
					fileWriter.close();
					removeOldFiles(filePath);
					disableTimeout();
					break;
				}
				// Resend history
				socket.send(history);
			}
		}

	}

	/**
	 * This method sends the file from the client to the server
	 * 
	 * @param filePath
	 *            The path of the file to send
	 * @throws IOException
	 *             Throws an I/O exception
	 */
	private void clientSendFile(String filePath) throws IOException {
		int timeouts = 0;
		setTimeout(CLIENT_SOCKET_TIMEOUT_TIME);
		do {
			try {
				socket.receive(new DatagramPacket(new byte[ACK_PACKET_SIZE], ACK_PACKET_SIZE));
				System.out.println("Ack from server recieved!");
				break;
			} catch (SocketTimeoutException se) {
				if (++timeouts > 10) {
					System.out.println("Server or network is down");
					disableTimeout();
					return;
				}
				socket.send(history);
				System.out.println("Ack not recieved!");
			} catch (IOException e) {
				disableTimeout();
				error("Error transmitting packet: " + e);
				return;
			}
		} while (true);

		int packetNumber = 1; // first packet
		boolean lastSegment = false, isLastPacketThrough = false;
		FileInputStream fileReader = null;

		try {
			filePath = filePath.replace("\u0000", "");
			File theFile = new File(filePath);
			try {
				fileReader = new FileInputStream(theFile);
			} catch (IOException e) {
				error("Failed to load file");
				return;
			}
		} catch (Exception e) {
			error("File reading error sending a chunk");
			return;
		}

		setTimeout(SERVER_SOCKET_TIMEOUT_TIME);
		while (true) {
			isLastPacketThrough = false;
			try {
				lastSegment = prepareBuffer(packetNumber, fileReader.available());
				packet = bufferToPacket(fileReader);
				do {
					socket.send(packet);
					ackPacket = new DatagramPacket(new byte[ACK_PACKET_SIZE], ACK_PACKET_SIZE);
					socket.receive(ackPacket);
					if (getAckNumber(ackPacket) == packetNumber) {
						isLastPacketThrough = true;
						packetNumber++;
					}
				} while (!isLastPacketThrough);
				if (lastSegment) {
					fileReader.close();
					disableTimeout();
					return;
				}
			} catch (SocketTimeoutException se) {
				System.out.println("Server disapeared or network is down");
				disableTimeout();
				return;
			} catch (IOException e) {
				disableTimeout();
				error("Error transmitting packet: " + e);
				return;
			}
		} // while
	} // clientSendFile

	/**
	 * This method disables the timeouts on the socket
	 */
	private void disableTimeout() {
		try {
			socket.setSoTimeout(0);
		} catch (SocketException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * This method sets the socket timeout to the number of miliseconds
	 * specified
	 *
	 * @param mili
	 *            The number of miliseconds for the timeout
	 */
	private void setTimeout(int mili) {
		try {
			socket.setSoTimeout(mili);
		} catch (SocketException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Determine the buffer size needed to send the file
	 * 
	 * @param packetNumber
	 *            The number of the packet
	 * @param fileLength
	 *            The length of the file
	 * @return A true/false value indicating whether or not this is the last
	 *         segment
	 */
	private boolean prepareBuffer(int packetNumber, int fileLength) {
		if (fileLength < BUFFER_SIZE - DATA_PACKET_HEADER_SIZE) {
			buffer = ByteBuffer.allocate(fileLength + DATA_PACKET_HEADER_SIZE);
			buffer.putInt(packetNumber);
			buffer.put((byte) 1); // last segment
			return true;
		} else {
			buffer = ByteBuffer.allocate(BUFFER_SIZE);
			buffer.putInt(packetNumber);
			buffer.put((byte) 0); // not last segment
			return false;
		}
	}

	/**
	 * This method coverts a file buffer to a packet
	 * 
	 * @param fio
	 *            The file input stream
	 * @return The packet from the data stream
	 * @throws IOException
	 *             Throws an I/O exception
	 */
	private DatagramPacket bufferToPacket(FileInputStream fio) throws IOException {
		byte[] temp = new byte[buffer.remaining()];
		System.out.println("getting " + (temp.length) + " of " + fio.available() + " from the file");
		fio.read(temp, 0, temp.length);
		buffer.put(temp);
		return new DatagramPacket(buffer.array(), buffer.array().length, destAddress, destPort);

	}

	/**
	 * This method gets the ack number from a packet
	 * 
	 * @param ackPacket
	 *            The packet from which to extract the ack number
	 * @return The ack number as an integer
	 */
	private int getAckNumber(DatagramPacket ackPacket) {
		byteMagic = ByteBuffer.allocate(ACK_PACKET_SIZE);
		byteMagic.put(Arrays.copyOf(ackPacket.getData(), ackPacket.getLength()));
		byteMagic.flip();
		System.out.println("Recieved Ack #: " + byteMagic.getInt());
		byteMagic.rewind();
		return byteMagic.getInt();
	}

}
